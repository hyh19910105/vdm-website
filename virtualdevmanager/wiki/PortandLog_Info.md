# Adapter & Log Info

### JIRA adapter

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5656|**/vagrant/Adapters/jiraserver.log**|

### Bamboo adapter

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5657|**/vagrant/Adapters/bamboo.log**|

### Bitbucket adapter

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5658|**/var/tmp/bitbucket.log**|

* All required python modules should be specified in **/vagrant/Adapters/requirements.txt**