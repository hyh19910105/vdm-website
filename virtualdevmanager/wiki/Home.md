# Welcome

Welcome to VDM wiki! This is the home page which contains all information about VDM.

## Table of Contents
* [Architecture](Architecture.md)  
This document provide a high level architecture view of the VDM application, includes web-framework, technology used. It is important to know system components before you build your own adapter.

* [How To Build A New Adapter](Adapter.md)   
This document provide detailed description about how to build your own adapter plugin for VDM. Include what are the functions you need to implement and development handout.

* [Port & Log Info](Port&Log_Info.md)   
This document provide critical information for debugging purpose. Include where to look for log files and which port number is available. 

* [CouchDB Schema](CouchDB_schema.md)   
This document provide detailed descriotion about structure of CouchDB that VDM is using. 

* [Python Coding Style Guide](Python_Coding_Style_Guide.md)   
This document is for developers who want to participate in VDM project and the Python coding standard they need to follow. 

* [Node.js Coding Style Guide](Node.js_Coding_Style_Guide.md)   
This document is for developers who want to participate in VDM project and the Node.js coding standard they need to follow. 

* [Issue Tracker Specification](IssueTrackerSpecification.md)   
This document provide detailed graph explaination about issue tracker type of adapters (JIRA, Fogbugz, etc.)

Cheers!   
Team VDM