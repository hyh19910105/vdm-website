# Release Notes

## 6.0.0 July 2, 2017
### Feature Added
* VDM-410 Adding coding style guide
* VDM-390 Adding callback function to async request branches
* VDM-396 Create the initial version of couchdb_schema.md

### Bug Fixes
* VDM-392  change the UI of adapter list branches
* Fix formatting of the case statistics text in the upper right hand corner
* Updated readme wording per tim and also updated the wording per tim's request on the project overview page
* Adding some updated screenshots that we can use in the readme
* VDM-392 Add label for bitbucket projects

## 5.0.0 June 28, 2017
### Feature Added
* VDM-408 Bitbucket adapter commit, auth and cancel complete, update data
* VDM-360 New web framework to handle concurrent request for JIRA adapter
* VDM-390  - Adding update logic whenever a user click add project

### Bug Fixes
* VDM-401 Fix bugs in displaying the repo activity chart
* Removing build activity and adding case activity
* VDM-401 Delete fields not needed
* VDM-392 Add adapter selection in “add project” page



## 4.0.0 June 19, 2017
### Feature Added
* VDM-361 implement that if there is no project in the vdm, the JIRA adapter will not pull the data from the JIRA server.
* VDM-360 New web framework to handle concurrent request for JIRA adapter

### Bug Fixes
* VDM-361 Fix the bug: unestimated issue is shown as estimated in the issue status chart
* VDM-361 Add “sprint issue status per team member chart”
* VDM-361 Fix chart layout and ignore the start date of VDM_Sprint 4
* VDM-361 Change JIRA adapter to storing project data after all the issues are pulled
* VDM-365 Removing commented out jira adapter lines and fixing the font color. also adding a disclaimer on dashboard.html
* VDM-381 Fixed project bar color problem and fixed formatting slightly
Branches


## 3.0.0 June 5, 2017
### Feature Added
None, static fix.

### Bug Fixes
* VDM-352 Static analysis bug fixing for the dataprocessing/charts.py and the vdm-main module Branches
* VDM-321 Fixing the hardcoded adapter issue Branches
* VDM-351 Static analysis fix
* VDM-374 Converting readme docx to ReadMe.md file
* VDM-365 Implement filtering out not needed project ids in the jiraserver.py and optimize initialization
* VDM-381 Set the timezone to America/New\_York for the vagrant box and convert timestamps passed by JIRA server from UTC time to local time (America/New\_York) in JIRA adapter
* VDM-361 Change chart names and labels

## 2.0.0 June 5, 2017
### Feature Added
None, static fix.

### Bug Fixes
* VDM-339 Comment three functions in dataprocessing/issues/charts.py
* Add logs folder

## 1.0.0 May 31, 2017
### Feature Added
Initial release, nothing added.

### Bug Fixes
None.